module NumToText(numToString) where

import qualified Data.List as Ls
import qualified Data.List.Split as Sp
import qualified Data.Map.Strict as Mp
import qualified Data.Maybe as My

-- Holds the names of the of the powers of 10 for which it is true that: [10^x | x <- [3,6..66]]
powerNames :: [[Char]]
powerNames = ["","Thousand","Million","Billion","Trillion","Quadrillion","Quintillion","Sextillion","Septillion","Octillion","Nonillion","Decillion","Undecillion","Duodecillion","Tredecillion","Quattuordecillion","Quindecillion","Sexdecillion","Septendecillion","Octodecillion","Novemdecillion","Vigintillion","Unvingintillion"]

powerMap :: Mp.Map [Char] [Char]
powerMap = Mp.fromList $ zipper 3 3 powerNames

oneToNineteenNames :: [[Char]]
oneToNineteenNames = ["Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"]

oneToNineteenMap :: Mp.Map [Char] [Char]
oneToNineteenMap = Mp.fromList $ zipper 0 1 oneToNineteenNames

tenNames :: [[Char]]
tenNames = ["Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"]

tenMap :: Mp.Map [Char] [Char]
tenMap = Mp.fromList $ zipper 2 1 tenNames

--make maybe more generic
zipper :: Int -> Int -> [b] -> [([Char],b)]
zipper from offset blist = zipWith zipp [from,from + offset..(from - 1 + offset * Ls.length blist)] blist
                          where zipp a b = (show a,b)
    
numToString :: Integer -> [Char]
numToString num 
                | num < 0 = "Minus " ++ res
                | num == 0 = My.fromJust $ Mp.lookup "0" oneToNineteenMap
                | otherwise = res
                where stri = show $ abs num
                      inp = map (concat) $ createTriplets stri
                      res = runTriplets inp

runTriplets :: [[Char]] -> [Char]      
runTriplets triplets 
                    | len == 0 || len > length powerNames = ""
                    | len == 1 = processTriplet $ head triplets
                    | otherwise = foldr folder [] $ filter (filt) $ zipWith zipper triplets ( reverse $ take len powerNames)
                    where len = length triplets
                          zipper trip pow = (processTriplet trip, pow)
                          filt (a,_) = a /= ""
                          folder (t,p) acc = if p == "" 
                                                then t ++ acc
                                                else if null acc 
                                                        then t ++ " " ++ p
                                                        else t ++ " " ++ p ++ ", " ++ acc
                                 
createTriplets inpNum = reverse $ map reverse $ (map (Sp.chunksOf 1) $ Sp.chunksOf 3 (reverse inpNum))

-- for single digit chars
numcharToString :: [Char] -> Int -> [Char]
numcharToString digitChar tripletPos 
                              | tripletPos == 2 = My.fromMaybe "" $ Mp.lookup digitChar tenMap
                              | otherwise = My.fromMaybe "" $ Mp.lookup digitChar oneToNineteenMap
   
processTriplet :: [Char] -> [Char]                     
processTriplet triplet
                   | le == 3 = filt [processHundred $ head triplet, processTen $ tail triplet]
                   | le == 2 = filt [processTen $ triplet]
                   | le == 1 = filt [processOne $ head triplet]
                   | otherwise = ""
                   where le = length triplet
                         filt = Ls.unwords . filter (/="")

processOne :: Char -> [Char]
processOne el
               | el == '0' = ""
               | otherwise = numcharToString [el] 1
               
processTen :: [Char] -> [Char]               
processTen trip
               | el == '0' && el2 == '0' = ""
               | el == '0' && el2 /= '0' = numcharToString [el2] 1
               | el == '1' = numcharToString [el, el2] 1
               | el2 /= '0' = numcharToString [el] 2 ++ "-" ++ processOne el2
               | otherwise = numcharToString [el] 2
               where el = head trip
                     el2 = head $ tail trip
                   
processHundred :: Char -> [Char]
processHundred ch
               | ch == '0' = ""
               | otherwise = numcharToString [ch] 3 ++ " Hundred"
