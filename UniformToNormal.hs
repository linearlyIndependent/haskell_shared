module UniformToNormal
              (
                main
              )
              where
              
              
import Control.Monad
import Data.List
import Data.Maybe
import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Diagrams
import System.IO
import System.Random
import Text.Read


main :: IO()
main = >> handleInput
       >>= \res -> if (res == Nothing) 
                     then putStrLn "Invalid input, aplication closed." 
                     else newStdGen
                          >>= \gen1 -> newStdGen
                          >>= \gen2 -> (plotPoints $ generatePoints (fromJust res) gen1 gen2)
                          >> putStrLn "Plot generated {result.png}."
                       
       
-- | Reads, parses and evaluates input, returns IO(Nothing) if one step fails.
handleInput :: IO(Maybe ((Double,Double),Int))
handleInput = putStrLn "Enter an inclusive range on which to generate the values:"
              >> putStrLn "pattern: (from,to)"
              >> (readMaybe <$> getLine)
              >>= \range -> putStrLn "Enter the number of values to be generated:"
              >> (readMaybe <$> getLine)
              >>= \num -> return . join $ helper <$> range <*> num
              where helper (from,to) number
                        | from <= to && number >= 0 = Just ((from,to), number)
                        | otherwise = Nothing
                        
                        
-- | Generates the points.
generatePoints :: (RandomGen g) => ((Double,Double),Int) -> g -> g -> [(Double,Double)]
generatePoints ((from,to), number) g1 g2 = take number . filter helper $ zip (randomRs (from,to) g1) (randomRs (0,0.4) g2)
                                          where avg = (from+to)/2
                                                e = exp 1
                                                helper (x,y) = y < e**( ((-1)*(x - avg)**2) / 2) / sqrt(2*pi)
                                                               -- TODO: implement Zigurrat for better performance.
-- | Plots the points.
plotPoints :: [(Double,Double)] -> IO()
plotPoints values = toFile def "result.png" $ do
                                layout_title .= "Result:"
                                setColors [opaque blue]
                                plot (points "values" values)
