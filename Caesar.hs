{-| 
This module provides functions to encode and decode a given string using the Caesar Encryption.

 * Valid characters are UTF-8 characters in the range ['0'..'9'], ['a'..'z'], ['A'..'Z']. Other characters remain the same and won't be decoded.
 * Accepts positive, negative numbers and 0 as offset.

-}
module Caesar
    (-- * Encoding
    encode  
    -- * Decoding
    -- ** With key
    , decode  
    -- ** Without key (Cracking)
    , decodeCrack
    ) where  

import Data.Char
import qualified Data.ByteString.Char8 as C
  
{-| 
Encodes a string with the caesar encryption, with the given offset.

 * Valid characters are UTF-8 characters in the range ['0'..'9'], ['a'..'z'], ['A'..'Z']. Other characters remain the same and won't be encoded.
 * Accepts positive, negative numbers and 0 as offset.
 
>>> encode 7 "If he had anything confidential to say, he wrote it in cipher, that is, by so changing the order of the letters of the alphabet, that not a word could be made out."
"pm ol ohk hufaopun jvumpkluaphs av zhf, ol dyval pa pu jpwoly, aoha pz, if zv johunpun aol vykly vm aol slaalyz vm aol hswohila, aoha uva h dvyk jvbsk il thkl vba."
-} 
encode :: Int -> String -> String
encode num = foldr (\ x acc -> (shiftChar num $ toLower x) : acc) []

{-| 
Cracks a string encoded with the caesar encryption.

 * Valid characters are UTF-8 characters in the range ['0'..'9'], ['a'..'z'], ['A'..'Z']. Other characters remain the same and won't be decoded.
 * Calculates the most possible key and uses that to decode the message.
 
>>> decodeCrack "pm ol ohk hufaopun jvumpkluaphs av zhf, ol dyval pa pu jpwoly, aoha pz, if zv johunpun aol vykly vm aol slaalyz vm aol hswohila, aoha uva h dvyk jvbsk il thkl vba."
"If he had anything confidential to say, he wrote it in cipher, that is, by so changing the order of the letters of the alphabet, that not a word could be made out."
-} 
decodeCrack :: String -> String
decodeCrack str = encode (getHighestMatch str) str

{-| 
Decodes a string encoded with the caesar encryption.

 * Valid characters are UTF-8 characters in the range ['0'..'9'], ['a'..'z'], ['A'..'Z']. Other characters remain the same and won't be decoded.
 * Accepts positive, negative numbers and 0 as offset.
 
>>> decode 7 "pm ol ohk hufaopun jvumpkluaphs av zhf, ol dyval pa pu jpwoly, aoha pz, if zv johunpun aol vykly vm aol slaalyz vm aol hswohila, aoha uva h dvyk jvbsk il thkl vba."
"If he had anything confidential to say, he wrote it in cipher, that is, by so changing the order of the letters of the alphabet, that not a word could be made out."
-}  
decode :: Int -> String -> String
decode num = encode (negate num)

-- | The reference table for the English alphabet (exclusive numbers)
referenceTable :: [Float]
referenceTable = [ 8.2, 1.5, 2.8, 4.3, 12.7, 2.2, 2.0, 6.1, 7.0, 0.2, 0.8, 4.0, 2.4, 6.7, 7.5, 1.9, 0.1, 6.0, 6.3, 9.1, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1 ]

-- | Shifts a list with a given length by a given offset, doesn't check data validity
shiftBy :: Int -> Int -> [a] -> [a] 
shiftBy shi len list = drop realShi list ++ take realShi list 
  where realShi = len - mod shi len

-- | Generates the value table for a given string
genCharTable :: String -> [Float]
genCharTable str =  [  ( (fromIntegral $ C.count ch bstr) / le) | ch <- ['a'..'z']]
  where bstr = C.pack $ map (toLower) str
        le = fromIntegral $ C.length bstr

-- | ASCII value for the letter 'a'
base :: Int
base = ord 'a'

-- | ASCII value for the letter 'z'
ceil :: Int
ceil = ord 'z'

-- | Shifts the given character by the given offset, excludes all non-english-alphabet characters.
shiftChar :: Int -> Char -> Char
shiftChar num ch 
  | elem ch ['a'..'z'] = chr . manageShift (mod num 26) $ ord ch
  | otherwise = ch

-- | Handles the cases when the shift goes through min/max borders.
manageShift :: Int -> Int -> Int
manageShift offset charnum  
  | val > ceil = base + val - ceil - 1
  | val < base = ceil - (base - val)
  | otherwise = val
  where val = offset + charnum

-- | Gets the shift offset which produces a table from the original string that's the most similar to the reference table.
getHighestMatch :: String -> Int
getHighestMatch str = fst $ foldr1 (\ (indX, valX) (indAcc, valAcc) -> if valX < valAcc then (indX, valX) else (indAcc, valAcc)) $ map (compareTables) shiftTables 
  where tbl = genCharTable str
        shiftTables = map (\ x -> (x, (shiftBy x 26 tbl)) ) [0..25] -- [[(ind, [values])],..]

-- | Compares a generated table to the reference table and returns the similarity to it as a float
compareTables :: (Int, [Float]) -> (Int, Float)
compareTables (ind, tbl) = (ind, sum . map (\ (x, refx) -> ((x - refx)^2) / refx) $ zip tbl referenceTable)
