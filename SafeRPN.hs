-- | This module defines a safe Reverse Polish Notation calculator.
module SafeRPN( main
              , PolishEntry(..)
              , toList
              , solveRPN
              , readEntry
              ) where

import System.IO
import Control.Monad
import Data.Char
import Data.Maybe
import Data.Stack
import qualified Text.Read as Tr
import qualified Data.Map as Mp
import qualified Data.List as Ls



main = do
    putStrLn "Enter the reverse polish notation:"
    print . solveRPN =<< getLine

    

infixOp :: (Fractional a, Floating a) => Mp.Map String (a -> a -> a)
infixOp = Mp.fromList [("+",(+)),("-",(-)),("*",(*)),("/",(/)),("^",(**))]

prefixOp1 :: (Fractional a, Floating a) => Mp.Map String (a -> a)
prefixOp1 = Mp.fromList [("ln",log)]

prefixOpList :: (Fractional a, Floating a) => Mp.Map String ([a] -> a)
prefixOpList = Mp.fromList [("sum",sum), ("product",product)]
    
    
    
data PolishEntry t = Number
                        {
                            getVal :: t
                        }
                    |InfixOP (t -> t -> t)
                    |PrefixOP (t -> t)
                    |PrefixOPList ([t] -> t)
 


-- | Solves an RPN.
solveRPN :: (Fractional a, Floating a, Read a) => String -> Maybe (a)
solveRPN string = (getVal . snd) <$> ((stackPop) =<< foldl (applyPolish) (Just stackNew) (words string))


{-|
Applies the current entry to the stack content.
The function which is mapped over the input list.
-}
applyPolish :: (Fractional a, Floating a, Read a) => Maybe (Stack (PolishEntry a)) -> String ->  Maybe (Stack (PolishEntry a))
applyPolish mStack inp = mStack 
                         >>= \stack -> (readEntry inp) 
                         >>= \newInp -> applyOP newInp stack

 
                    
-- | Tries to read a "PolishEntry".
readEntry :: (Fractional a, Floating a, Read a) => String -> Maybe (PolishEntry a)
readEntry inp
    | Mp.member inp infixOp = InfixOP <$> Mp.lookup inp infixOp
    | Mp.member inp prefixOp1 = PrefixOP <$> Mp.lookup inp prefixOp1
    | Mp.member inp prefixOpList = PrefixOPList <$> Mp.lookup inp prefixOpList
    | otherwise = Number <$> Tr.readMaybe inp
    
    

-- | Applies the current entry to the stack content.
applyOP :: PolishEntry a -> Stack (PolishEntry a) -> Maybe (Stack (PolishEntry a))
applyOP (Number num) stack = return . stackPush stack $ Number num
applyOP (InfixOP op) stack = (stackPop stack) 
                             >>= \(stack1,el1) -> stackPop stack1 
                             >>= \(stack2,el2) -> return . stackPush stack2 . Number $ (op) (getVal el2) (getVal el1)
applyOP (PrefixOP op) stack = stackPop stack 
                              >>= \(stack1,el1) -> return . stackPush stack1 . Number $ op (getVal el1)        
applyOP (PrefixOPList op) stack = return . stackPush stackNew . Number . (op) . map (getVal) $ toList stack

   

-- | Converts a stack to a list.
toList :: Stack a -> [a]
toList stack
    | stackIsEmpty stack = []
    | otherwise = res : (toList stack2)
     where (stack2,res) = fromJust $ stackPop stack
