{-# LANGUAGE OverloadedStrings #-}

module Whois(main) where

import Lib
import Data.Maybe
import Data.Text
import Data.Text.Encoding
import Data.Text.IO as IO
import qualified Data.ByteString as S
import qualified Data.ByteString.Char8 as S8
import qualified Data.ByteString.Lazy as L
import Network.Simple.TCP

-- desigarized biatch
main :: IO ()
main =
    IO.putStrLn "Enter a address to lookup:" >>
    fmap (encodeUtf8 . strip) IO.getLine >>= \lookupAddress ->
    connect "whois.iana.org" "43" $ \(socket,remAddr) ->
    IO.putStrLn "Connected to whois" >>
    IO.putStrLn "------------------" >>
    send socket ( S.append lookupAddress (encodeUtf8 "\r\n")) >>
    recv socket 8192 >>= \response ->
    S.putStr $ fromMaybe "error" response
            

main' :: IO ()
main' = do
    IO.putStrLn "Enter a address to lookup:"
    lookupAddress <- fmap (encodeUtf8 . strip) IO.getLine
    connect "whois.iana.org" "43" $ \(socket,remAddr) -> do
            IO.putStrLn "Connected to whois"
            IO.putStrLn "------------------"
            send socket $ S.append lookupAddress $ encodeUtf8 "\r\n"
            response <- recv socket 8192
            S.putStr $ fromMaybe "error" response
