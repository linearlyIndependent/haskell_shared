{-| 
This module provides functions to encode and decode a given string from / into Morse codes.

 * Valid characters are defined in the 'morseBaseTable' 'Mp.Map'. Other characters automatically trigger an 'exception'.

-}
module Morse(-- * Encoding
            charToMorse,
            encode,
            -- * Decoding
            morseToChar,
            decode,
            -- * Morse table
            morseBaseTable
            ) 
            where

import qualified Data.Map as Mp
import qualified Data.Maybe as My
import qualified Data.List as Ls
import qualified Data.Char as Ch


morseBaseTable :: [(Char, [Char])]
morseBaseTable = [ ('A', ".-"),('B', "-..."),('C', "-.-."),('D', "-.."),('E', "."),('F', "..-."),('G', "--."),('H', "...."),('I', ".."),('J', ".---"),('K', "-.-"),('L', ".-.."),('M', "--"),('N', "-."),('O', "---"),('P', ".--."),('Q', "--.-"),('R', ".-."),('S', "..."),('T', "-"),('U', "..-"),('V', "...-"),('W', ".--"),('X', "-..-"),('Y', "-.--"),('Z', "--.."),('1', ".----"),('2', "..---"),('3', "...--"),('4', "....-"),('5', "....."),('6', "-...."),('7', "--..."),('8', "---.."),('9', "----."),('0', "-----"),('.', ".-.-.-"),(',', "--..--"),('\'', ".----."),(':', "---..."),(';', "-.-.-."),('"', ".-..-."),('@', ".--.-."),('\n', "._._"),(' ', "_") ]

charMorseMap ::Mp.Map Char [Char]
charMorseMap = Mp.fromList morseBaseTable


-- make with flip for for from list
morseCharMap ::Mp.Map [Char] Char
morseCharMap = foldr (\(f,s) acc -> Mp.insert s f acc) Mp.empty morseBaseTable

-- make with flip for for from list !!!!!!!!!!!!!!!!!
--morseCharMap' ::Mp.Map Char [Char]
--morseCharMap' = flip(Mp.fromList) morseBaseTable

charToMorse :: Char -> [Char]
charToMorse ch = My.fromMaybe "" $ Mp.lookup (Ch.toUpper ch) charMorseMap
--charToMorse ch = My.fromMaybe [] $ Mp.lookup ch charMorseMap

morseToChar :: [Char] -> Char
morseToChar mr = My.fromMaybe ' ' $ Mp.lookup mr morseCharMap
--morseToChar mr = My.fromMaybe base.Char.Empty $ Mp.lookup mr morseCharMap

encode :: [Char] -> [Char]
encode inp = Ls.unwords $ Ls.map charToMorse inp
--encode inp = Ls.unwords $ Ls.concatMap (Ls.map charToMorse) $ Ls.words inp

decode :: [Char] -> [Char]
decode inp = Ls.map morseToChar $ Ls.words inp
